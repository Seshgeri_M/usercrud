const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    _id: String,
    email: String,
    firstName: String,
    middleName: String,
    lastName: String,
    phone: String
});

module.exports = mongoose.model('usercrud', NoteSchema);
